#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <sys/wait.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <wordexp.h>
#include <sys/types.h> 
#include <dirent.h>
#include <stdbool.h>

#define PROMPT_ENDING "# "
#define NELEMS(x)  (sizeof(x) / sizeof(x[0]))

extern char **environ;

const char* BUILT_IN[] = {"","exit","mycd","mypwd","export", "myls", "concr"};

char* get_prompt_str();
int cmd_prompt(char*, wordexp_t*);
void free_input(char*, wordexp_t*);
void exec_builtin(int, char*[]);
void exec_cmd(char*[], bool);
int is_builtin(char*[]);
void cmd_export(char *argv[]);
void cmd_myls(char *argv[]);
char d_typetochar(unsigned char d_type);
void cmd_concr(char *[]);


/*
 * Function: main
 * --------------
 * Main command loop.
 */
int main(int argc, char* argv[])
{
    char* input_str = NULL;
    int cmd = 0;
    wordexp_t cmd_argv;

    signal(SIGINT, SIG_IGN);

    while(1)
    {
        if(!cmd_prompt(input_str, &cmd_argv))
        {
            free_input(input_str, &cmd_argv);
            continue;
        }

        if ((cmd = is_builtin(cmd_argv.we_wordv)))
            exec_builtin(cmd, cmd_argv.we_wordv);   // exec built_in command
        else
            exec_cmd(cmd_argv.we_wordv, false);            // exec command

        free_input(input_str, &cmd_argv);
    }
}

char* get_prompt_str()
{
    char* path  = malloc(sizeof(char)*258);
    getcwd(path, 258);
    return strcat(path, PROMPT_ENDING);
}

/*
 * Function:  cmd_prompt
 * -----------------------
 * Handles command line input using readline and wordexp.
 * Prints the prompt and waits for user's input returning 0 if no command was
 * issued, or 1 otherwise with cmd_argv holding the parsed command line.
 */
int cmd_prompt(char* input_str, wordexp_t* cmd_argv)
{
    char* prompt_str = get_prompt_str();
    if(!(input_str = readline(prompt_str)))     // command input
    {
        printf("exit\n");
        exit(0);
    }

    free(prompt_str);

    int error = wordexp(input_str, cmd_argv, WRDE_SHOWERR | WRDE_UNDEF);

    if(error)
    {
        switch (error) {
            case WRDE_BADCHAR:
                printf("Bad character. Illegal occurrence of newline or one of |, &, ;, <, >, (, ), {, }\n");
                break;
            case WRDE_BADVAL:
                printf("Undefined variable\n");
                break;
            case WRDE_SYNTAX:
                printf("Syntax error\n");
                break;
            default:
                printf("Unknown error\n");
        }

         return 0;
    }

    if (cmd_argv->we_wordc == 0) // no input
    {
        return 0;
    }

    add_history(input_str);

    return 1;
}

/*
 * Function: exec_cmd
 * ------------------
 * Execute given command and waits till it's completed. cmd_argv[0] must shields
 * the command to be executed, cmd_argv[i] (i > 0) shields the command arguments.
 */
void exec_cmd(char* cmd_argv[], bool concr)
{
    int status;
    int child_pid = fork();

    if (child_pid != 0) // parent process
    {
        if (!concr)
            waitpid(child_pid, &status, 0);
    }
    else // child process
    {
        execvp(cmd_argv[0], cmd_argv); // execute PROGRAM
        perror("");
        exit(0);
    }
}

/*
 * Function: exec_builtin
 * ----------------------
 * Executes built-in commmands registered in BUILT_IN array.
 */
void exec_builtin(int cmd, char* cmd_argv[])
{
    char path[255];

    switch(cmd)
    {
        case 1:
            exit(0);
            break;
        case 2:
            chdir(cmd_argv[1]);
            break;
        case 3:
            getcwd(path, 255);
            printf("%s\n", path);
            break;
        case 4: 
            cmd_export(cmd_argv);
            break; 
        case 5: 
            cmd_myls(cmd_argv);
            break; 
        case 6: 
            cmd_concr(cmd_argv); 
            break;
        default:
            printf("-bsh: command not found\n");
    }

    return;
}

/*
 * Function: cmd_export
 * Built in version of export command. Shows a list of the current environment 
 * variables if no arguments passed, or exports a variable with its given value
 * to the environment. 
 */
void cmd_export(char *cmd_argv[])
{
    if (cmd_argv[1] == NULL)  // si no pasa parametros entonces muestro las variables del entorno
    {
        char **env = environ; 
        while ( *env != NULL ) 
        {
            printf("%s\n",*env);
            env++;
        }
        env = NULL;
    } else {
        char* aux = malloc(sizeof(char)* strlen(cmd_argv[1]));
        sprintf(aux, "%s", cmd_argv[1]); // putenv() no funcionaba con memoria en stack 
        putenv(aux);
        free(aux);
    }
}

void cmd_myls(char *cmd_argv[]) 
{
    struct dirent *dir_info  ; 

    char* path = malloc(sizeof(char)*255); 
    if (cmd_argv[1] == NULL)
    {
        getcwd(path, 256) ; 
        printf("no arguments? listing current directory (%s) \n\n", path);
    } else {
        sprintf(path, "%s", cmd_argv[1]);
        printf("listing: %s \n\n", path);
    }

    DIR* dir = opendir(path);
    dir_info = readdir(dir);

    while ( (dir_info = readdir(dir)) )
    {
        printf("%c---------  %s\n", d_typetochar(dir_info->d_type),  dir_info->d_name);
        dir_info++;
    }

    closedir(dir);
}

char d_typetochar(unsigned char d_type)
{
    char type_chr; 
    switch(d_type) {
        case DT_BLK: 
            type_chr = 'b';
            break; 
        case DT_CHR: 
            type_chr = 'c';
            break; 
        case DT_DIR      : 
            type_chr = 'd';
            break; 
        case DT_FIFO: 
            type_chr = 'p';
            break; 
        case DT_LNK: 
            type_chr = 'l';
            break; 
        case DT_REG: 
            type_chr = '-';
            break; 
        case DT_SOCK: 
            type_chr = 's';
            break; 
        case DT_UNKNOWN: 
            type_chr = '?';
            break; 
        default: 
            type_chr = '?'; 
            break; 
    };
    return (type_chr);
}

void cmd_concr(char *cmd_argv[])
{
    char **aux = cmd_argv; 

    printf("Executing %s in background\n\n", cmd_argv[1]);

    aux++;
    exec_cmd(aux, true);
}

/*
 * Function: is_builtin
 * ----------------------
 * Finds out if the command is a built-in one, registered in BUILT_IN array and
 * returns the command position.
 */
int is_builtin(char* cmd_argv[])
{
    for(int i = 1; i < NELEMS(BUILT_IN); i++)
        if (!strcmp(cmd_argv[0], BUILT_IN[i]))
            return i;

    return 0;
}


// utility functions
void free_input(char* cmd_str, wordexp_t* cmd_argv)
{
    free(cmd_str);
    wordfree(cmd_argv);
}
